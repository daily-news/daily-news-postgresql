# Check if there's an active container
	echo "Checking if exists a DailyNewsPostgreSql container..."
	DOCKER_CONTAINER_DAILYNEWS_POSTGRESQL_ID=$(docker ps | grep "DailyNewsPostgreSql" | sed -e 's/^\(.\{12\}\).*/\1/')

	# Check if there is an active container
	if [ ! -z "$DOCKER_CONTAINER_DAILYNEWS_POSTGRESQL_ID" ]; then
		echo "There are active containers. Deleting..."
		docker rm -f $(docker ps | grep "DailyNewsPostgreSql" | sed -e 's/^\(.\{12\}\).*/\1/')
	fi

echo "Creating new containers..."
docker-compose up -d